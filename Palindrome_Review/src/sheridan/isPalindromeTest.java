package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class isPalindromeTest {

	@Test
	public void testIsPalindrome() {
		assertTrue("Invalid Value for palindrome", isPalindrome.isPalindrome2("anna"));
	}
	@Test
	public void testIsPalindromeBoundaryIn() {
		assertTrue("Invalid Value for palindrome", isPalindrome.isPalindrome2("a"));
	}
	@Test
	public void testIsPalindromeBoundaryOut() {
		assertFalse("This is a palindrome", isPalindrome.isPalindrome2("ann"));
	}
	@Test
	public void testIsPalindromeNegative() {
		assertFalse("This is a palindrome", isPalindrome.isPalindrome2("abc"));
	}
}
