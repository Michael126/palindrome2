package sheridan;

public class isPalindrome{
	public static  boolean isPalindrome2(String input)
	{
		input = input.toUpperCase();
		input = input.replaceAll(" ", "");
		
		
		for(int i = 0, j = input.length() -1 ; i < j; i++, j--)
		{
			if(input.charAt(i) != input.charAt(j))
			{
				return false;
			}
			
		}
        
  
        //string is a palindrome 
        return true; 
		
	}
	public static void main(String[] args) {
		System.out.println("Is anna a palindrome? " +isPalindrome2("anna"));
	}

}
